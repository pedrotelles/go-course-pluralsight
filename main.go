package main

import (
	"fmt"
	"go-functions/simplemath"
)

func main() {

	answer, err := simplemath.Divide(6, 0)

	if err != nil {
		fmt.Printf("An error has ocurred: %s\n", err.Error())
	} else {
		fmt.Printf("%f\n", answer)

	}

	total := simplemath.Sum(12.2, 14, 16, 22.4)

	fmt.Printf("Total of sum: %f\n", total)

	numbers := []float64{12.2, 14, 16, 22.4}
	total2 := simplemath.Sum(numbers...)

	fmt.Printf("Total of sum2: %f\n", total2)

	sv := simplemath.NewSemanticVersion(1, 2, 3)

	fmt.Println(sv.String())

}
